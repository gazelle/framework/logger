# Logger

Logger is a component used by applications or modules to record context and processing information in
the server logs for maintenance and debugging purposes.

The Logger module is part of the Gazelle Framework and offers two APIs and the underlying implementation to access log methods from a client application.
This module is composed by two components : **Logger Service** and **Logger Interceptor**.

The **Logger Service** module offers two APIs :

* An API used to instantiate a logger using a factory. The factory (GazelleLoggerFactory) puts in cache instantiated loggers (GazelleLogger).
* An API offering methods covering six log levels which can be used to log information with several parameters.

The **Logger Interceptor** module is used to automatically intercept method calls (entry and exit of each called method) at TRACE and exceptions at debug level.

## Pre-requisites

The **Logger** module is usable with **JDK 11**.

## How to use

### Add the library to the class path

Logger module is available in IHE-Europe's [Nexus repository](https://gazelle.ihe.net/nexus).

It can be integrated in a project as a maven dependency :

```xml
<!-- Logger Model -->
<dependency>
    <groupId>net.ihe.gazelle</groupId>
    <artifactId>framework.logger-service</artifactId>
    <version>1.0.0-SNAPSHOT</version>
</dependency>

<!-- Logger Interceptor -->
<dependency>
    <groupId>net.ihe.gazelle</groupId>
    <artifactId>framework.logger-interceptor</artifactId>
    <version>1.0.0-SNAPSHOT</version>
</dependency>
```

The **Logger Interceptor** dependency, when integrated, requires to integrate the following plugin :

```xml
<plugin>
    <groupId>com.nickwongdev</groupId>
    <artifactId>aspectj-maven-plugin</artifactId>
    <version>1.12.1</version>
    <configuration>
        <complianceLevel>11</complianceLevel>
        <source>11</source>
        <target>11</target>
        <showWeaveInfo>true</showWeaveInfo>
        <verbose>true</verbose>
        <Xlint>ignore</Xlint>
        <encoding>UTF-8</encoding>
    </configuration>
    <executions>
        <execution>
            <goals>
                <!-- use this goal to weave all your main classes -->
                <goal>compile</goal>
                <!-- use this goal to weave all your test classes -->
                <goal>test-compile</goal>
            </goals>
        </execution>
    </executions>
</plugin> 
```

### Verify your deployment environment

If you are deploying your application in an application server, 
it should use the application server logging system as the logger implementation. 

Gazelle is using a Wildfly server, please refer to [Wildfly logging configurations documentation](https://docs.jboss.org/author/display/WFLY10/Logging+Configuration) for more information.

Therefore, if you are deploying your application in a server or a container which is not providing a logger, 
it must then provide a logger implementation. Log4j is the recommended implementation of a logger.

Log4j and SLF4j can be added as dependencies in project POMs using the following examples :

```xml
<dependency>
   <groupId>org.apache.logging.log4j</groupId>
   <artifactId>log4j-slf4j-impl</artifactId>
   <version>2.13.0</version>
   <scope>test</scope>
</dependency>

```

Please refer to the Log4j official documentation for more information on this logging implementation : [Log4j Official Documentation](https://logging.apache.org/log4j/2.x/).

### API method calls

#### Message logging methods

All the methods defined in GazelleLogger and GazelleLoggerFactory interfaces can be called by the client application.
Each log level as a dedicated method in GazelleLogger interface.
They can be called like in the following examples :

```java_holder_method_tree
GazelleLogger logger = GazelleLoggerFactory.getInstance().getLogger(MyClass.class);

logger.trace("This is a trace logging method");
logger.debug("This is a debug logging method");
logger.info("This is an info logging method");
logger.warn("This is a warn logging method");
logger.error("This is an error logging method");
logger.fatal("This is a fatal logging method");
```

#### Parametrized logging methods

Some method prototypes allow the use of arguments inside the message of the log entry. Due to performance purposes, the message is 
parametrized with a "{}" formatting anchor. The main goal is to avoid concatenation of string (message + argument(s))) 
as this is very costly for the reason invoked before. The provided arguments will be included at the place of the anchor in the message.
The following example enhances this behavior :

Calling 

```java_holder_method_tree
GazelleLogger logger = GazelleLoggerFactory.getInstance().getLogger("testLoggerParametrized");

logger.trace("This is a {} logging method","trace");
```
will lead to the following message in the logs : 
"This is a trace logging method".

#### Exception logging methods
 
Finally, several methods in the API offer the possibility to log exception (throwable argument in the example below), 
attached to the log entry (with a message or a parametrized message) :
 
```java_holder_method_tree
GazelleLogger logger = GazelleLoggerFactory.getInstance().getLogger("testLoggerWithException");

logger.fatal(new GazelleException(),"Here is a an exception");
logger.fatal(new GazelleException(),"Here is a an exception {}",argument);
```

#### Detection of log level

The developer does not need to check the log level whenever a method from the API is called : it
 is already handled by the library.
 
For example, you should not use the following lines in your client application :

```java_holder_method_tree
GazelleLogger logger = GazelleLoggerFactory.getInstance().getLogger("testLogger");

if (logger.debugEnabled()){
    logger.debug("This is a debug logging method");
}
```
