package net.ihe.gazelle.framework.loggerinterceptor.adapter;

import net.ihe.gazelle.framework.loggerservice.application.GazelleLogger;
import net.ihe.gazelle.framework.loggerservice.application.GazelleLoggerFactory;
import net.ihe.gazelle.framework.loggerservice.adapter.GazelleLoggerFactoryImpl;

/**
 * This class defines the aspect interceptor for logging purposes.
 *
 * @author lcy
 */
public aspect LogInterceptor {


    private GazelleLoggerFactory loggerFactory = new GazelleLoggerFactoryImpl();
    private final GazelleLogger LOG = loggerFactory.getLogger("LogAspect");


    pointcut publicMethodExecuted(): execution(public * *(..));

    pointcut methodExecutedWithException(): execution(public * *(..));

    /**
     *  automatic logging of entrance in function at trace level
     */
    before(): publicMethodExecuted() {
        LOG.trace("Enters on method: {}", thisJoinPoint.getSignature());
    }

    /**
     *  automatic logging of outgoing in function at trace level
     */
    after(): publicMethodExecuted() {
        LOG.trace("Exits method: {}", thisJoinPoint.getSignature());
    }

    /**
     *  automatic logging of exception at debug level
     */
    after() throwing (Throwable e) : execution(* *(..)){

        String method = thisJoinPoint.getSignature().getName();
        LOG.debug("Threw in the method " + method + " : ", e);

    }

}