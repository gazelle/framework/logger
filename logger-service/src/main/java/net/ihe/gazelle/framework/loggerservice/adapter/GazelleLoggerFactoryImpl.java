package net.ihe.gazelle.framework.loggerservice.adapter;

import net.ihe.gazelle.framework.loggerservice.application.GazelleLogger;
import net.ihe.gazelle.framework.loggerservice.application.GazelleLoggerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Implementation of GazelleLoggerFactory based on SLF4J. Created Loggers are kept in cache.
 *
 * @author nbt
 */
public class GazelleLoggerFactoryImpl implements GazelleLoggerFactory {

    private static Map<String, GazelleLogger> gazelleLoggersMap = new ConcurrentHashMap<>();

    /**
     * {@inheritDoc}
     */
    @Override
    public GazelleLogger getLogger(String name) {
        return gazelleLoggersMap.computeIfAbsent(name, n -> new GazelleLoggerImpl(getLoggerByName(n)));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public GazelleLogger getLogger(Class<?> clazz) {
        return gazelleLoggersMap.computeIfAbsent(clazz.getName(), c -> new GazelleLoggerImpl(getLoggerByName(c)));
    }

    /**
     * Create an instance of {@link Logger} or retrieve an already created one based the {@code name} attribute
     * (created when there is no existing logger with this name).
     *
     * @param name   Name of the logger which will be either created on retrieved
     *
     * @return the Logger from the utility mapped with the {@code name}.
     */
    private Logger getLoggerByName(String name) {
        return LoggerFactory.getLogger(name);
    }
}
