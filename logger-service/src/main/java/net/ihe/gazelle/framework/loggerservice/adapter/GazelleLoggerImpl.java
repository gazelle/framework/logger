package net.ihe.gazelle.framework.loggerservice.adapter;

import net.ihe.gazelle.framework.loggerservice.application.GazelleLogger;
import org.slf4j.Logger;

/**
 * Implementation of GazelleLogger based on SLF4J.
 *
 * @author nbt
 */
public class GazelleLoggerImpl implements GazelleLogger {

    private Logger logger;

    public static final String RELATED_EXCEPTION = "Related exception: ";
    private static final String FATAL = "FATAL ";

    /**
     * Constructor. It will need an implementation of {@link Logger}.
     *
     * @param logger Instantiated Logger from the logging utility implementation
     */
    GazelleLoggerImpl(Logger logger) {
        this.logger = logger;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void trace(String message) {
        logger.trace(message);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void debug(String message) {
        logger.debug(message);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void info(String message) {
        logger.info(message);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void warn(String message) {
        logger.warn(message);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void error(String message) {
        logger.error(message);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void fatal(String message) {
        if (logger.isErrorEnabled()) {
            message = "{} " + message;
            logger.error(message, FATAL);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void trace(String format, Object... arguments) {
        logger.trace(format,arguments);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void debug(String format, Object... arguments) {
        logger.debug(format,arguments);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void info(String format, Object... arguments) {
        logger.info(format,arguments);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void warn(String format, Object... arguments) {
        logger.warn(format,arguments);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void error(String format, Object... arguments) {
        logger.error(format,arguments);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void fatal(String format, Object... arguments) {
        if (logger.isErrorEnabled()) {
            format = FATAL + format + "{}";
            logger.error(format, arguments);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void trace(Throwable throwable, String message) {
        logger.trace(message,throwable);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void debug(Throwable throwable, String message) {
        logger.debug(message,throwable);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void info(Throwable throwable, String message) {
        logger.info(message,throwable);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void warn(Throwable throwable, String message) {
        logger.warn(message,throwable);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void error(Throwable throwable, String message) {
        logger.error(message,throwable);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void fatal(Throwable throwable, String message) {
        if (logger.isErrorEnabled()) {
            message = FATAL + message + "{}";
            logger.error(message, throwable);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void trace(Throwable throwable, String format, Object... arguments) {
        logger.trace(format,arguments);
        logger.trace(RELATED_EXCEPTION,throwable);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void debug(Throwable throwable, String format, Object... arguments) {
        logger.debug(format,arguments);
        logger.debug(RELATED_EXCEPTION,throwable);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void info(Throwable throwable, String format, Object... arguments) {
        logger.info(format,arguments);
        logger.info(RELATED_EXCEPTION,throwable);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void warn(Throwable throwable, String format, Object... arguments) {
        logger.warn(format,arguments);
        logger.warn(RELATED_EXCEPTION,throwable);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void error(Throwable throwable, String format, Object... arguments) {
        logger.error(format,arguments);
        logger.error(RELATED_EXCEPTION,throwable);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void fatal(Throwable throwable, String format, Object... arguments) {
        if (logger.isErrorEnabled()) {
            format = FATAL + format + "{}";
            logger.error(format,arguments);
            logger.error(RELATED_EXCEPTION,throwable);
        }
    }
}
