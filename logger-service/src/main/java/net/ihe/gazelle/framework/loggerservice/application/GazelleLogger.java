package net.ihe.gazelle.framework.loggerservice.application;

/**
 * This interface defines methods which can be accessed by client applications in order to log events among 6 log levels.
 *
 * @author nbt
 */
public interface GazelleLogger {

    /**
     * This method is used to log a message at trace log level.
     *
     * @param message   The message to be logged
     */
    void trace(String message);

    /**
     * This method is used to log a message at debug log level.
     *
     * @param message   The message to be logged
     */
    void debug(String message);

    /**
     * This method is used to log a message at info log level.
     *
     * @param message   The message to be logged
     */
    void info(String message);

    /**
     * This method is used to log a message at warn log level.
     *
     * @param message   The message to be logged
     */
    void warn(String message);

    /**
     * This method is used to log a message at error log level.
     *
     * @param message   The message to be logged
     */
    void error(String message);

    /**
     * This method is used to log a message at fatal log level.
     *
     * @param message   The message to be logged
     */
    void fatal(String message);

    /**
     * This method is used to log a parametrized message including arguments at trace log level. The format parameter must include "{}" formatting
     * anchor to integrate arguments in the displayed message.
     *
     * <u>Example :</u>
     *
     * <b>trace("This is a {} log message","trace")</b> will log the following message :
     * <p>
     * "This is a <b>trace</b> log message"
     *
     * @param format    The message to be logged
     * @param arguments Arguments included in the parametrized logged message
     */
    void trace(String format, Object... arguments);

    /**
     * This method is used to log a parametrized message including arguments at debug log level.
     * The format parameter must include "{}" formatting anchor to integrate arguments in the displayed message.
     *
     * <u>Example :</u>
     *
     * <b>debug("This is a {} log message","debug")</b> will log the following message :
     *
     * "This is a <b>debug</b> log message"
     *
     * @param format   The message to be logged
     * @param arguments   Arguments included in the parametrized logged message
     */
    void debug(String format, Object... arguments);

    /**
     * This method is used to log a parametrized message including arguments at info log level.
     * The format parameter must include "{}" formatting anchor to integrate arguments in the displayed message.
     *
     * <u>Example :</u>
     *
     * <b>info("This is an {} log message","info")</b> will log the following message :
     *
     * "This is an <b>info</b> log message"
     *
     * @param format   The message to be logged
     * @param arguments   Arguments included in the parametrized logged message
     */
    void info(String format, Object... arguments);

    /**
     * This method is used to log a parametrized message including arguments at warn log level.
     * The format parameter must include "{}" formatting anchor to integrate arguments in the displayed message.
     *
     * <u>Example :</u>
     *
     * <b>warn("This is a {} log message","warn")</b> will log the following message :
     *
     * "This is a <b>warn</b> log message"
     *
     * @param format   The message to be logged
     * @param arguments   Arguments included in the parametrized logged message
     */
    void warn(String format, Object... arguments);

    /**
     * This method is used to log a parametrized message including arguments at error log level.
     * The format parameter must include "{}" formatting anchor to integrate arguments in the displayed message.
     *
     * <u>Example :</u>
     *
     * <b>error("This is an {} log message","error")</b> will log the following message :
     *
     * "This is an <b>error</b> log message"
     *
     * @param format   The message to be logged
     * @param arguments   Arguments included in the parametrized logged message
     */
    void error(String format, Object... arguments);

    /**
     * This method is used to log a parametrized message including arguments at fatal log level.
     * The format parameter must include "{}" formatting anchor to integrate arguments in the displayed message.
     *
     * <u>Example :</u>
     *
     * <b>fatal("This is a {} log message","fatal")</b> will log the following message :
     *
     * "This is a <b>fatal</b> log message"
     *
     * @param format   The message to be logged
     * @param arguments   Arguments included in the parametrized logged message
     */
    void fatal(String format, Object... arguments);

    /**
     * This method is used to log a message with a linked exception at trace log level.
     *
     * @param throwable  The exception linked with the logged message
     * @param message   The message to be logged
     */
    void trace(Throwable throwable, String message);

    /**
     * This method is used to log a message with a linked exception at debug log level.
     *
     * @param throwable  The exception linked with the logged message
     * @param message   The message to be logged
     */
    void debug(Throwable throwable, String message);

    /**
     * This method is used to log a message with a linked exception at info log level.
     *
     * @param throwable  The exception linked with the logged message
     * @param message   The message to be logged
     */
    void info(Throwable throwable, String message);

    /**
     * This method is used to log a message with a linked exception at warn log level.
     *
     * @param throwable  The exception linked with the logged message
     * @param message   The message to be logged
     */
    void warn(Throwable throwable, String message);

    /**
     * This method is used to log a message with a linked exception at error log level.
     *
     * @param throwable  The exception linked with the logged message
     * @param message   The message to be logged
     */
    void error(Throwable throwable, String message);

    /**
     * This method is used to log a message with a linked exception at fatal log level.
     *
     * @param throwable  The exception linked with the logged message
     * @param message   The message to be logged
     */
    void fatal(Throwable throwable, String message);

    /**
     * This method is used to log a parametrized message including arguments with a linked exception at trace log level.
     * The format parameter must include "{}" formatting anchor to integrate arguments in the displayed message.
     *
     * <u>Example :</u>
     *
     * <b>trace("This is a {} log message","trace")</b> will log the following message :
     *
     * "This is a <b>trace</b> log message"
     *
     * @param throwable  The exception linked with the logged message
     * @param format   The message to be logged
     * @param arguments   Arguments included in the parametrized logged message
     */
    void trace(Throwable throwable, String format, Object... arguments);

    /**
     * This method is used to log a parametrized message including arguments with a linked exception at debug log level.
     * The format parameter must include "{}" formatting anchor to integrate arguments in the displayed message.
     *
     * <u>Example :</u>
     *
     * <b>debug("This is a {} log message","debug")</b> will log the following message :
     *
     * "This is a <b>debug</b> log message"
     * 
     * @param throwable  The exception linked with the logged message
     * @param format   The message to be logged
     * @param arguments   Arguments included in the parametrized logged message
     */
    void debug(Throwable throwable, String format, Object... arguments);

    /**
     * This method is used to log a parametrized message including arguments with a linked exception at info log level.
     * The format parameter must include "{}" formatting anchor to integrate arguments in the displayed message.
     *
     * <u>Example :</u>
     *
     * <b>info("This is an {} log message","info")</b> will log the following message :
     *
     * "This is an <b>info</b> log message"
     *
     * @param throwable  The exception linked with the logged message
     * @param format   The message to be logged
     * @param arguments   Arguments included in the parametrized logged message
     */
    void info(Throwable throwable, String format, Object... arguments);

    /**
     * This method is used to log a parametrized message including arguments with a linked exception at warn log level.
     * The format parameter must include "{}" formatting anchor to integrate arguments in the displayed message.
     *
     * <u>Example :</u>
     *
     * <b>warn("This is a {} log message","warn")</b> will log the following message :
     *
     * "This is a <b>warn</b> log message"
     *
     * @param throwable  The exception linked with the logged message
     * @param format   The message to be logged
     * @param arguments   Arguments included in the parametrized logged message
     */
    void warn(Throwable throwable, String format, Object... arguments);

    /**
     * This method is used to log a parametrized message including arguments with a linked exception at error log level.
     * The format parameter must include "{}" formatting anchor to integrate arguments in the displayed message.
     *
     * <u>Example :</u>
     *
     * <b>error("This is an {} log message","error")</b> will log the following message :
     *
     * "This is an <b>error</b> log message"
     *
     * @param throwable  The exception linked with the logged message
     * @param format   The message to be logged
     * @param arguments   Arguments included in the parametrized logged message
     */
    void error(Throwable throwable, String format, Object... arguments);

    /**
     * This method is used to log a parametrized message including arguments with a linked exception at fatal log level.
     * The format parameter must include "{}" formatting anchor to integrate arguments in the displayed message.
     *
     * <u>Example :</u>
     *
     * <b>fatal("This is a {} log message","fatal")</b> will log the following message :
     *
     * "This is a <b>fatal</b> log message"
     *
     * @param throwable  The exception linked with the logged message
     * @param format   The message to be logged
     * @param arguments   Arguments included in the parametrized logged message
     */
    void fatal(Throwable throwable, String format, Object... arguments);
}
