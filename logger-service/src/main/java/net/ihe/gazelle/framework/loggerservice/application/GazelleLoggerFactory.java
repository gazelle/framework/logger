package net.ihe.gazelle.framework.loggerservice.application;

import net.ihe.gazelle.framework.loggerservice.adapter.GazelleLoggerFactoryImpl;

/**
 * This API provides methods to instantiate a logger using a log factory.
 *
 * @author nbt
 */
public interface GazelleLoggerFactory {

    /**
     * Create an instance of {@link GazelleLoggerFactory}
     *
     * @return the GazelleLoggerFactory instance.
     */
    static GazelleLoggerFactory getInstance() {
        // Architecture Constraint violation authorized. Direct call to adapter from application layer for easy syntax in client applications.
        return new GazelleLoggerFactoryImpl();
    }

    /**
     * Get an instance of {@link GazelleLogger} based on the {@code name} attribute.
     * It does not always create a new instance of GazelleLogger as the instance is temporarily saved in a cache
     * after being created for performance benefits.
     *
     * @param name   Name which will be affected to the logger
     *
     * @return the GazelleLogger created with {@code name}
     */
    GazelleLogger getLogger(String name);

    /**
     * Get an instance of {@link GazelleLogger} based on the {@code clazz} attribute.
     * It does not always create a new instance of GazelleLogger as the instance is temporarily saved in a cache
     * after being created for performance benefits.
     *
     * @param clazz   Class which will be affected to the logger
     *
     * @return the GazelleLogger created with {@code clazz}
     */
    GazelleLogger getLogger(Class<?> clazz);
}
