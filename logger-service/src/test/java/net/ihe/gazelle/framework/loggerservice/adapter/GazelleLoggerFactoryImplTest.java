package net.ihe.gazelle.framework.loggerservice.adapter;

import net.ihe.gazelle.framework.loggerservice.application.GazelleLogger;
import net.ihe.gazelle.framework.loggerservice.application.GazelleLoggerFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.LoggerFactory;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Tests for {@link GazelleLoggerFactoryImpl}
 *
 * @author nbt
 */
public class GazelleLoggerFactoryImplTest {

    GazelleLoggerFactory gazelleLoggerFactory;

    /**
     * Initialize tests by creating an instance of {@link GazelleLoggerFactoryImpl}
     */
    @BeforeEach
    public void init() {
        gazelleLoggerFactory = GazelleLoggerFactory.getInstance();
    }

    @Test
    public void testGetLoggerByName() {
        String name = "testGetLoggerByName";
        GazelleLogger gazelleLogger = gazelleLoggerFactory.getLogger("testGetLoggerByName");
        assertNotNull(gazelleLogger, "GazelleLogger when created with name must not be null");
        assertEquals(LoggerFactory.getLogger(name).getName(), name,"The instantiated logger must have the same name as the given one");
    }

    @Test
    public void testGetLoggerByClassName() {
        GazelleLogger gazelleLogger = gazelleLoggerFactory.getLogger(GazelleLoggerFactoryImplTest.class);
        assertNotNull(gazelleLogger, "GazelleLogger when created with class must not be null");
        assertEquals(LoggerFactory.getLogger(GazelleLoggerFactoryImplTest.class).getName(), this.getClass().getCanonicalName(),"The instantiated logger must have the class name as the given canonical one");
    }

    @Test
    public void testCacheBetweenLoggerInstances() {
        GazelleLogger logger1 = gazelleLoggerFactory.getLogger(GazelleLoggerFactoryImplTest.class);
        GazelleLogger logger2 = gazelleLoggerFactory.getLogger(GazelleLoggerFactoryImplTest.class.getName());

        assertSame(logger1, logger2, "GazelleLogger instantiated must be put in cache by the factory");
    }
}
