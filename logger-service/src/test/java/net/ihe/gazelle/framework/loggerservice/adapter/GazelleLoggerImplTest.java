package net.ihe.gazelle.framework.loggerservice.adapter;

import net.ihe.gazelle.framework.loggerservice.application.GazelleLogger;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Tests for {@link GazelleLoggerImpl}
 *
 * @author nbt
 */
public class  GazelleLoggerImplTest {

    private static GazelleLogger gazelleLogger;
    private static Path logFilePath = Paths.get("./src/test/resources/app.out");
    private static String fatalString = "FATAL";
    private static String fatalErrorMessage = "Fatal should be written in the file";

    /**
     * Initialize tests by creating an instance of {@link GazelleLoggerImplTest}
     */
    @BeforeAll
    static void init() {
        Logger logger = LoggerFactory.getLogger(GazelleLoggerImplTest.class);
        gazelleLogger = new GazelleLoggerImpl(logger);
    }
    /**
     * Once all the tests have been executed, this method deletes the file created by the log4j FileAppender
     */
    @AfterAll
    static void after() {
        try {
            Files.delete(logFilePath);
        } catch (IOException e) {
            gazelleLogger.error("Unable to perform the deletion of the file located at {}",logFilePath);
            gazelleLogger.error("An IO Exception occured during the deletion of the file",e);
        }
    }

    @Test
    public void testTraceLogsOneArgument() {
        String traceMessage = "Here is a trace log for method trace(message)";
        gazelleLogger.trace(traceMessage);
        assertTrue(checkInFile(traceMessage),"TraceMessage should be written in the file");
    }

    @Test
    public void testTraceTwoArguments() {
        String traceMessage = "Here is a trace log for method {}";
        String traceArgument = "trace(message,arguments)";
        String traceMessageTwoArguments = "Here is a trace log for method trace(message,arguments)";
        gazelleLogger.trace(traceMessage,traceArgument);
        assertTrue(checkInFile(traceMessageTwoArguments),"TraceMessage with argument should be written in the file");
    }

    @Test
    public void testTraceTwoArgumentsThrowable() {
        String traceArgument = "trace(throwable,message)";
        Throwable throwable = new Throwable(new NullPointerException());
        gazelleLogger.trace(throwable,traceArgument);
        assertTrue(checkInFile(traceArgument),"TraceArgument should be written in the file");
        assertTrue(checkInFile(throwable.getLocalizedMessage()),"Exception message for trace log method should be written in the file");
    }

    @Test
    public void testTraceThreeArgumentsThrowable() {
        String traceFormat = "Here is a trace log for method {}";
        String traceArgument = "trace(throwable,format,arguments)";
        String traceMessageTwoArguments = "Here is a trace log for method trace(throwable,format,arguments)";
        Throwable throwable = new Throwable(new NullPointerException());
        gazelleLogger.trace(throwable,traceFormat,traceArgument);
        assertTrue(checkInFile(traceMessageTwoArguments),"TraceMessage with argument should be written in the file");
        assertTrue(checkInFile(throwable.getLocalizedMessage()),"Exception message for trace log method should be written in the file");
    }

    @Test
    public void testDebugLogsOneArgument() {
        String debugMessage = "Here is a debug log for method debug(message)";
        gazelleLogger.debug(debugMessage);
        assertTrue(checkInFile(debugMessage),"DebugMessage should be written in the file");
    }

    @Test
    public void testDebugLogsTwoArguments() {
        String debugMessage = "Here is a debug log for method {}";
        String debugArgument = "debug(message,arguments)";
        String debugMessageTwoArguments = "Here is a debug log for method debug(message,arguments)";
        gazelleLogger.debug(debugMessage,debugArgument);
        assertTrue(checkInFile(debugMessageTwoArguments),"DebugMessage with argument should be written in the file");
    }

    @Test
    public void testDebugLogsTwoArgumentsThrowable() {
        String debugArgument = "debug(throwable,message)";
        Throwable throwable = new Throwable(new NullPointerException());
        gazelleLogger.trace(throwable,debugArgument);
        assertTrue(checkInFile(debugArgument),"DebugArgument should be written in the file");
        assertTrue(checkInFile(throwable.getLocalizedMessage()),"Exception message for debug log method should be written in the file");
    }

    @Test
    public void testDebugThreeArgumentsThrowable() {
        String debugFormat = "Here is a debug log for method {}";
        String debugArgument = "debug(throwable,format,arguments)";
        String debugMessageTwoArguments = "Here is a debug log for method debug(throwable,format,arguments)";
        Throwable throwable = new Throwable(new NullPointerException());
        gazelleLogger.debug(throwable,debugFormat,debugArgument);
        assertTrue(checkInFile(debugMessageTwoArguments),"DebugMessage with argument should be written in the file");
        assertTrue(checkInFile(throwable.getLocalizedMessage()),"Exception message for debug log method should be written in the file");
    }

    @Test
    public void testInfoLogsOneArgument() {
        String infoMessage = "Here is an info log for method info(message)";
        gazelleLogger.info(infoMessage);
        assertTrue(checkInFile(infoMessage),"InfoMessage should be written in the file");
    }

    @Test
    public void testInfoLogsTwoArguments() {
        String infoMessage = "Here is an info log for method {}";
        String infoArgument = "info(message,arguments)";
        String infoMessageTwoArguments = "Here is an info log for method info(message,arguments)";
        gazelleLogger.info(infoMessage,infoArgument);
        assertTrue(checkInFile(infoMessageTwoArguments),"InfoMessage with argument should be written in the file");
    }

    @Test
    public void testInfoLogsTwoArgumentsThrowable() {
        String infoArgument = "info(throwable,message)";
        Throwable throwable = new Throwable(new NullPointerException());
        gazelleLogger.trace(throwable,infoArgument);
        assertTrue(checkInFile(infoArgument),"InfoArgument should be written in the file but is not");
        assertTrue(checkInFile(throwable.getLocalizedMessage()),"Exception message for info log method should be written in the file");
    }

    @Test
    public void testInfoThreeArgumentsThrowable() {
        String infoFormat = "Here is an info log for method {}";
        String infoArgument = "info(throwable,format,arguments)";
        String infoMessageTwoArguments = "Here is an info log for method info(throwable,format,arguments)";
        Throwable throwable = new Throwable(new NullPointerException());
        gazelleLogger.info(throwable,infoFormat,infoArgument);
        assertTrue(checkInFile(infoMessageTwoArguments),"InfoMessage with argument should be written in the file");
        assertTrue(checkInFile(throwable.getLocalizedMessage()),"Exception message for info log method should be written in the file");
    }

    @Test
    public void testWarnLogsOneArgument() {
        String warnMessage = "Here is a warn log for method warn(message)";
        gazelleLogger.warn(warnMessage);
        assertTrue(checkInFile(warnMessage),"WarnMessage should be written in the file");
    }

    @Test
    public void testWarnLogsTwoArguments() {
        String warnMessage = "Here is a warn log for method {}";
        String warnArgument = "warn(message,arguments)";
        String warnMessageTwoArguments = "Here is a warn log for method warn(message,arguments)";
        gazelleLogger.warn(warnMessage,warnArgument);
        assertTrue(checkInFile(warnMessageTwoArguments),"WarnMessage with argument should be written in the file");
    }
    @Test
    public void testWarnLogsTwoArgumentsThrowable() {
        String warnArgument = "warn(throwable,message)";
        Throwable throwable = new Throwable(new NullPointerException());
        gazelleLogger.trace(throwable,warnArgument);
        assertTrue(checkInFile(warnArgument),"WarnArgument should be written in the file but is not");
        assertTrue(checkInFile(throwable.getLocalizedMessage()),"Exception message for warn log method should be written in the file");
    }

    @Test
    public void testWarnThreeArgumentsThrowable() {
        String warnFormat = "Here is a warn log for method {}";
        String warnArgument = "warn(throwable,format,arguments)";
        String warnMessageTwoArguments = "Here is a warn log for method warn(throwable,format,arguments)";
        Throwable throwable = new Throwable(new NullPointerException());
        gazelleLogger.warn(throwable,warnFormat,warnArgument);
        assertTrue(checkInFile(warnMessageTwoArguments),"WarnMessage with argument should be written in the file");
        assertTrue(checkInFile(throwable.getLocalizedMessage()),"Exception message for warn log method should be written in the file");
    }

    @Test
    public void testErrorLogsOneArgument() {
        String errorMessage = "Here is an error log for method error(message)";
        gazelleLogger.error(errorMessage);
        assertTrue(checkInFile(errorMessage),"ErrorMessage should be written in the file");
    }

    @Test
    public void testErrorLogsTwoArguments() {
        String errorMessage = "Here is an error log for method {}";
        String errorArgument = "error(message,arguments)";
        String errorMessageTwoArguments = "Here is an error log for method error(message,arguments)";
        gazelleLogger.error(errorMessage,errorArgument);
        assertTrue(checkInFile(errorMessageTwoArguments),"ErrorMessage with argument should be written in the file");
    }

    @Test
    public void testErrorLogsTwoArgumentsThrowable() {
        String errorArgument = "error(throwable,message)";
        Throwable throwable = new Throwable(new NullPointerException());
        gazelleLogger.trace(throwable, errorArgument);
        assertTrue(checkInFile(errorArgument),"ErrorArgument should be written in the file");
        assertTrue(checkInFile(throwable.getLocalizedMessage()),"Exception message for error log method should be written in the file");
    }

    @Test
    public void testErrorThreeArgumentsThrowable() {
        String errorFormat = "Here is an error log for method {}";
        String errorArgument = "error(throwable,format,arguments)";
        String errorMessageTwoArguments = "Here is an error log for method error(throwable,format,arguments)";
        Throwable throwable = new Throwable(new NullPointerException());
        gazelleLogger.error(throwable,errorFormat,errorArgument);
        assertTrue(checkInFile(errorMessageTwoArguments),"ErrorMessage with argument should be written in the file");
        assertTrue(checkInFile(throwable.getLocalizedMessage()),"Exception message for error log method should be written in the file");
    }

    @Test
    public void testFatalLogsOneArgument() {
        String fatalMessage = "Here is a fatal log for method fatal(message)";
        gazelleLogger.fatal(fatalMessage);
        assertTrue(checkInFile(fatalMessage),"FatalMessage should be written in the file");
        assertTrue(checkInFile(fatalString),fatalErrorMessage);
    }

    @Test
    public void testFatalLogsTwoArguments() {
        String fatalMessage = "Here is a fatal log for method {}";
        String fatalArgument = "fatal(message,arguments)";
        String fatalMessageTwoArguments = "Here is a fatal log for method fatal(message,arguments)";
        gazelleLogger.fatal(fatalMessage, fatalArgument);
        assertTrue(checkInFile(fatalString),fatalErrorMessage);
        assertTrue(checkInFile(fatalMessageTwoArguments),"FatalMessage with argument should be written in the file");
    }

    @Test
    public void testFatalLogsTwoArgumentsThrowable() {
        String fatalArgument = "fatal(throwable,message)";
        Throwable throwable;
        throwable = new Throwable(new NullPointerException());
        gazelleLogger.fatal(throwable,fatalArgument);
        assertTrue(checkInFile(fatalArgument),"Fatal Argument should be written in the file");
        assertTrue(checkInFile(fatalString),fatalErrorMessage);
        assertTrue(checkInFile(throwable.getLocalizedMessage()),"Exception message for fatal log method should be written in the file");
    }

    @Test
    public void testFatalThreeArgumentsThrowable() {
        String fatalFormat = "Here is a fatal log for method {}";
        String fatalArgument = "fatal(throwable,format,arguments)";
        String fatalMessageTwoArguments = "Here is a fatal log for method fatal(throwable,format,arguments)";
        Throwable throwable = new Throwable(new NullPointerException());
        gazelleLogger.fatal(throwable,fatalFormat,fatalArgument);
        assertTrue(checkInFile(fatalMessageTwoArguments),"FatalMessage with argument should be written in the file");
        assertTrue(checkInFile(fatalString),fatalErrorMessage);
        assertTrue(checkInFile(throwable.getLocalizedMessage()),"Exception message for fatal log method should be written in the file");
    }

    private boolean checkInFile(String stringToFind) {
        try (Stream<String> input = Files.lines(logFilePath)){
            List<String> lines = input.filter(line -> line.contains(stringToFind)).collect(Collectors.toList());
            return !lines.isEmpty();
        } catch (IOException e) {
            gazelleLogger.error("Unable to verify log file content", e);
            return false;
        }
    }

}
